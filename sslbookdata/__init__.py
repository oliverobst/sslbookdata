__version__ = "0.1"

from .sslbookdata import load_digit1
from .sslbookdata import load_usps
from .sslbookdata import load_coil2
from .sslbookdata import load_bci
from .sslbookdata import load_g241c
from .sslbookdata import load_coil
from .sslbookdata import load_g241n
from .sslbookdata import load_secstr
from .sslbookdata import load_text

